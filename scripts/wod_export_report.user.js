﻿// ==UserScript==
// @id             wodlogexport@morning
// @name           [wod]批量导出战报
// @description    save wod reports.
// @namespace      morning
// @version        1.02
// @include        http*://*.world-of-dungeons.*/wod/spiel/*dungeon/report.php*
// @require        https://raw.githubusercontent.com/eligrey/Blob.js/master/Blob.js
// @require        https://raw.githubusercontent.com/eligrey/FileSaver.js/master/FileSaver.js
// @require        https://raw.githubusercontent.com/Stuk/jszip/master/dist/jszip.js
// @require        http://malsup.github.com/jquery.form.js
// @updateURL      https://bitbucket.org/wod/wod_export_report/raw/master/scripts/wod_export_report.user.js
// @downloadURL    https://bitbucket.org/wod/wod_export_report/raw/master/scripts/wod_export_report.user.js
// ==/UserScript==

(function() {

    function InsertButton(Node, Value, OnClick)
	{
        var newButton = document.createElement("input");
        newButton.setAttribute("type", "button");
        newButton.setAttribute("class", "button");
        newButton.setAttribute("value", Value);
        newButton.addEventListener("click", OnClick, false);
        Node.parentNode.insertBefore(newButton, Node.nextSibling);
    }
    
    function changeAllSelection(select)
    {
		var allCheckbox = document.getElementsByTagName("input");
        for (var i = 0; i < allCheckbox.length; ++i) 
        {
            var theCheckbox = allCheckbox[i];
            if(rVaule.Pattern_checkboxName.test(theCheckbox.getAttribute("name")))
            {
				theCheckbox.checked = select;
            }
        }
    }
    
    function selectAll()
    {
		if (!gIsWorking)
			changeAllSelection(true);
    }

    function cleartAll()
    {
		if (!gIsWorking)
			changeAllSelection(false);
    }

    function exportLog()
    {
		if (gIsWorking)
			return;
		gIsWorking = true;
        var allCheckbox = document.getElementsByTagName("input");
        gZip = new JSZip();
        gSelectedReport = [];
        for (var i = 0; i < allCheckbox.length; ++i) 
        {
            var theCheckbox = allCheckbox[i];
            if(rVaule.Pattern_checkboxName.test(theCheckbox.getAttribute("name")))
            {
				if(theCheckbox.checked)
                {
                   	gSelectedReport.push(theCheckbox);
                }
            }
        }
        
        if(gSelectedReport.length > 0)
        {           
            gTitle = window.prompt("输入战报名称","我的战报");
            gIndexDiv = gIndexTemplateDiv.cloneNode(true);
            var table = document.createElement("div");        
            gCurrentReport = gSelectedReport[0];
            GetStatPage();
    	}
        else
        {
            window.alert("没有选择任何战报");
        }
    }
    
    function GetStatPage()
    {
    	var queryString = $("form[name='the_form']").formSerialize() + "&IS_POPUP=1&" + gCurrentReport.getAttribute(rVaule.Text_Stat) + "=" + rVaule.Text_Stat;
        var XmlHttp = new XMLHttpRequest();
    
        XmlHttp.onreadystatechange = function ()
        {
            try	{
                if (XmlHttp.readyState == 4 && XmlHttp.status == 200)
                {
                    gResponseDiv.innerHTML = XmlHttp.responseText;
					infodiv.innerHTML = "保存战报：&nbsp;" + gTitle + "<br/>" + gCurrentReport.getAttribute("title") + " - 统计表";
					gZip.file(gCurrentReport.value + "/statistics.html",handlePage(gResponseDiv));
					GetItemPage();
				}
            }
            catch (e) {alert("GetItemPage XMLHttpRequest.onreadystatechange(): " + e);}
        };
    
        var URL = location.protocol + "//" + location.host + "/wod/spiel/dungeon/report.php";
    
        XmlHttp.open("POST", URL, true);
        XmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        XmlHttp.setRequestHeader("Content-length", queryString.length);
        XmlHttp.setRequestHeader("Connection", "close");
        XmlHttp.send(queryString);
    }

    function GetItemPage()
    {
    	var queryString = $("form[name='the_form']").formSerialize() + "&IS_POPUP=1&" + gCurrentReport.getAttribute(rVaule.Text_Item) + "=" + rVaule.Text_Item;
        var XmlHttp = new XMLHttpRequest();
    
        XmlHttp.onreadystatechange = function ()
        {
            try	{
                if (XmlHttp.readyState == 4 && XmlHttp.status == 200)
                {
                    gResponseDiv.innerHTML = XmlHttp.responseText;
					infodiv.innerHTML = "保存战报：&nbsp;" + gTitle + "<br/>" + gCurrentReport.getAttribute("title") + " - 获得物品";
					gZip.file(gCurrentReport.value + "/items.html",handlePage(gResponseDiv));
					GetLevelPage(1,1);
                }
            }
            catch (e) {alert("GetItemPage XMLHttpRequest.onreadystatechange(): " + e);}
        };
    
        var URL = location.protocol + "//" + location.host + "/wod/spiel/dungeon/report.php";
    
        XmlHttp.open("POST", URL, true);
        XmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        XmlHttp.setRequestHeader("Content-length", queryString.length);
        XmlHttp.setRequestHeader("Connection", "close");
        XmlHttp.send(queryString);
    }
    
    function GetLevelPage(nLevel,nRepPage)
	{
        var XmlHttp = new XMLHttpRequest();
    
        XmlHttp.onreadystatechange = function ()
            {
            try	{
                if (XmlHttp.readyState == 4 && XmlHttp.status == 200)
                    {
                        gResponseDiv.innerHTML = XmlHttp.responseText;
                        ReadLevelPage(nLevel,nRepPage);
                    }
                }
            catch (e) {alert("GetLevelPage XMLHttpRequest.onreadystatechange(): " + e);}
            };
    
         var URL = location.protocol + "//" + location.host + "/wod/spiel/dungeon/report.php" +
            "?cur_rep_id=" + gCurrentReport.value +
            "&gruppe_id=&current_level=" + nLevel +
			"&REPORT_PAGE=" + nRepPage +
            "&IS_POPUP=1";
    
        XmlHttp.open("GET", URL, true);
        XmlHttp.send(null);
	}
	
    function ReadLevelPage(nLevel,nRepPage)
    {
        if(nLevel == 1)
        {
            gCurrentReport.setAttribute("maxLevel", GetMaxLevel(gResponseDiv, 1));
            var rows = gIndexDiv.getElementsByTagName("tr");
            var row = rows[rows.length -1];
            row.parentNode.appendChild(row.cloneNode(true));
            
            row.setAttribute("class",gIndexRowclass);
            if(gIndexRowclass == "row0")
                gIndexRowclass = "row1";
            else
                gIndexRowclass = "row0";
            row.cells[0].innerHTML = replaceDate(gCurrentReport.getAttribute("reporttime"));
			row.cells[1].innerHTML = gCurrentReport.getAttribute("reportname");
            var cell = row.cells[2];
            
            cell.innerHTML = "";
            addIndexNewButton(cell,"统计表","document.location.href='" + gCurrentReport.value + "/statistics.html';");
            addIndexNewButton(cell,"获得物品","document.location.href='" + gCurrentReport.value + "/items.html';");
            for(var i=1;i<= gCurrentReport.getAttribute("maxLevel");i++)
            {
            	addIndexNewButton(cell,"层 " + i ,"document.location.href='" + gCurrentReport.value + "/level" + i + ".html';");
            }
            
        }
		var ret = GetRepPageInfo(gResponseDiv, [1, 1]);
		var nCurrRepPage = ret[0];
		var nMaxRepPage = ret[1];
		
		var maxLevel = gCurrentReport.getAttribute("maxLevel");
       	infodiv.innerHTML = "保存战报：&nbsp;" + gTitle + "<br/>" + gCurrentReport.getAttribute("title") + " - 第 " + nLevel + "/" + maxLevel + " 层详细资料";
        
        var theFileName = gCurrentReport.value + "/level" + nLevel + ".html";
		if(nCurrRepPage > 1)
		{
			theFileName = gCurrentReport.value + "/level" + nLevel + "_" + nCurrRepPage + ".html";
		}
			gZip.file(theFileName,handlePage(gResponseDiv));
        
        if(nLevel < maxLevel)
        {
            if(nCurrRepPage == nMaxRepPage)
				GetLevelPage(nLevel+1,1);
			else
				GetLevelPage(nLevel,nCurrRepPage +1);
        }
        else
        {
        	gCurrentReport.checked = false;
            for (var i = 0; i < gSelectedReport.length; ++i) 
			{
				var theCheckbox = gSelectedReport[i];
                if(theCheckbox.checked)
                {
                    gCurrentReport = theCheckbox;
                    GetStatPage();                   
                    return;
                }
			}
            handleIndexPage();
            infodiv.innerHTML = "保存战报：&nbsp;" + gTitle + "<br/>" + "生成Zip文件";
            var head = document.getElementsByTagName('head')[0].cloneNode(true);
            
            
            var indexStr = handlePage(head) + gIndexDiv.innerHTML;
            gZip.file("index.html",indexStr);
			var blob = gZip.generate({type:"blob"});
			saveAs(blob, "wodlog" + '_' + Math.random().toString(36).substr(2, 9) + ".zip");
			alert('zip文件生成完毕');
			infodiv.innerHTML = "";
            gResponseDiv.innerHTML = "";
			gIsWorking = false;
        }

    }
    
	function GetMaxLevel(Document, DefaultValue)
	{
		var ret = DefaultValue;

		var allInputs = Document.getElementsByTagName("input");
		for (var i = 0; i < allInputs.length; ++i)
		{
			var name = allInputs[i].getAttribute("name");

			if (rVaule.Pattern_level.test(name))
			{
				var levelnumber = Number(rVaule.Pattern_idNumber.exec(name)[1]);
				if(levelnumber > ret)
					ret = levelnumber;
			}
		}
		return ret;
	}

	// return: an array, [0]: nCurrRepPage, [1]: nMaxRepPage
	function GetRepPageInfo(Document, DefaultValue)
	{
		var ret = [DefaultValue[0], DefaultValue[1]];

		var SubPageIndexNode;
		var allInputs = Document.getElementsByTagName("input");
		for (var i = 0; i < allInputs.length; ++i)
			{
			if (allInputs[i].value.indexOf("=1=") !== -1)
				{
				SubPageIndexNode = allInputs[i];
				break;
				}
			}
		if (SubPageIndexNode == null)
			{
			return ret;
			};

		var bIndexEnd = false;
		while (!bIndexEnd)
		{
			var IndexPatt = /=([\d]+)=/;
			var target = (SubPageIndexNode.value == null)? SubPageIndexNode.firstChild.textContent:SubPageIndexNode.value;		
			var Result = IndexPatt.exec(target);
			var nCurrIndex = Number(Result[1]);

			if (SubPageIndexNode.className == "paginator_selected clickable")
				ret[0] = nCurrIndex;

			SubPageIndexNode = node_after(node_after(SubPageIndexNode));
			if (SubPageIndexNode == null || SubPageIndexNode.nodeName != "A")
				{
				ret[1] = nCurrIndex;
				bIndexEnd = true;
				}
		};

		return ret;
	}
    
    function handlePage(page)
    {
		if(gTitle == null)
            gTitle = "我的战报";
        page.getElementsByTagName('title')[0].innerHTML = gTitle;

        var h2 = page.getElementsByTagName("h2")[0];
        if(h2)
        {
        	h2.innerHTML = replaceDate(h2.innerHTML);
        }
        removePageDiv(page);
        replaceURL(page,"link","href");
        replaceURL(page,"script","src");
        replaceURL(page,"img","src");
        replaceURL(page,"a","href");
        replaceButton(page);
        return replaceOther(page.outerHTML);
    }
    
    function removePageDiv(page)
    {
        var divs = page.getElementsByTagName("div");
        for(var i=0;i<divs.length;i++)
        {
        	var theDiv = divs[i];
            if(theDiv.getAttribute("class") == "gadget popup")
            {
            	page.removeChild(theDiv);
                break;
            }
        }
        return page
    }
    
    function replaceURL(page,tag,attr)
    {
        var test_pattern = /^\//;
        var test1_pattern = /^#/
        var allLink = page.getElementsByTagName(tag);
        for(var i=0;i<allLink.length;i++)
        {
        	var link = allLink[i];
            if(link.hasAttribute(attr))
            {
                var uri = link.getAttribute(attr);
                if(!rVaule.pattern_http.test(uri))
                {
                    if(test_pattern.test(uri))
                    {
                        link.setAttribute(attr,location.origin + uri)
                    }
                    else if(!test1_pattern.test(uri))
                    {
                        var path = location.origin + location.pathname;
                        var m = path.match(/(.*)[\/\\]([^\/\\]+)\.\w+$/);
                        link.setAttribute(attr,m[1] + "/" + uri)
                   }
                }
            }
        }
    }
    
    function replaceButton(page)
    {
        var allInputs = page.getElementsByTagName("input");
        for (var i = 0; i < allInputs.length; ++i)
		{
            var name = allInputs[i].getAttribute("name");

            if (rVaule.Pattern_level.test(name))
			{
                var levelURL = "document.location.href='level" + rVaule.Pattern_idNumber.exec(name)[1] + ".html';";
				var button = allInputs[i];
                button.setAttribute("type","button");
                button.setAttribute("onclick",levelURL);                
			}
            if (rVaule.Pattern_item.test(name))
			{
                var levelURL = "document.location.href='items.html';";
				var button = allInputs[i];
                button.setAttribute("type","button");
                button.setAttribute("onclick",levelURL);                
			}
            if (rVaule.Pattern_stat.test(name))
			{
                var levelURL = "document.location.href='statistics.html';";
				var button = allInputs[i];
                button.setAttribute("type","button");
                button.setAttribute("onclick",levelURL);                
			}
            if (rVaule.Pattern_detail.test(name))
			{
                var levelURL = "document.location.href='level1.html';";
				var button = allInputs[i];
                button.setAttribute("type","button");
                button.setAttribute("onclick",levelURL);                
			}
            if (name == "overview" || name == "")
			{
                var levelURL = "document.location.href='../index.html';";
				var button = allInputs[i];
                button.setAttribute("type","button");
                button.setAttribute("onclick",levelURL);                
			}
        }
    }
    
    function replaceDate(sDate)
    {
        var today = new Date();
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate()-1);
        var ret = sDate.replace("今天",today.getFullYear() + '年' + (today.getMonth() + 1) + '月' + today.getDate() + '日');
        ret = ret.replace("昨天",yesterday.getFullYear() + '年' + (yesterday.getMonth() + 1) + '月' + yesterday.getDate() + '日');
        return ret;
    }
 
    function replaceOther(sHTML)
    {
    	var ret = sHTML.replace(/wodInitialize\(''/g,"wodInitialize('" + location.host + "'");
        ret = ret.replace(/wo\('\/wod/g,"wo('" + location.origin + "/wod");
        return ret;
    }

 	function prepareIndexPageTemplate()
    {
        var allRow = gIndexTemplateDiv.getElementsByTagName("tr");
        for(var j=allRow.length-1; j >1 ; --j)
        {
            var row = allRow[j];
            if(rVaule.Pattern_logRow.test(row.getAttribute("class")))
            {
                row.parentNode.removeChild(row);
            }
        }
    }

    function addIndexNewButton(cell,buttonText,url)
    {
        var newButton = document.createElement("input");
        newButton.setAttribute("type", "button");
        newButton.setAttribute("class", "button clickable");
        newButton.setAttribute("value", buttonText);
        newButton.setAttribute("onclick", url );
        cell.appendChild(newButton);
    }
    
    function handleIndexPage()
    {
        var allRow = gIndexDiv.getElementsByTagName("tr");
        var row = allRow[allRow.length-1];
        row.parentNode.removeChild(row);
    }
      
    function GetLocalContents(Contents)
	{
        function GetLanguageId()
        {
            var langText = null;
            var allMetas = document.getElementsByTagName("meta");
            for (var i = 0; i < allMetas.length; ++i)
            {
                if (allMetas[i].httpEquiv == "Content-Language")
                {
                    langText = allMetas[i].content;
                    break;
                }
            }
            if (langText == null)
                return false;
            
            switch (langText)
            {
                case "en":
                    return 0;
                case "cn":
                    return 1;
                default:
                    return null;
            }
        }
        
        var nLangId = GetLanguageId();
        if (nLangId == null)
            return null;
        
        if (Contents instanceof Object)
        {
            for (var name in Contents)
                Contents[name] = Contents[name][nLangId];
            return Contents;
        }
        else
            return null;
    }
    
    var gIndexRowclass = "row0";
    var gCurrentReport;
    var gZip;
    var rLocal;
    var infodiv;
    var gTitle;
	var gSelectedReport = [];
    var gIndexTemplateDiv;
    var gResponseDiv;
    var gIndexDiv;
	var gIsWorking = false;
    var rContents = {
        OrigText_H1_DungeonLog			: ["Battle Report",
                           "战报"],
        OrigText_Button_DungeonDetails	: ["details",
                           "详细资料"],
        Text_Button_Exportlog			: ["Export Log",
                           "导出战报"],
        Text_Button_SelectAll			: ["Select All",
                           "全选"],
        Text_Button_ClearAll			: ["Clear All",
                           "清除"]

        };

	var rVaule = {
        Text_Item 				: "items",
        Text_Stat				: "stats",
        Text_Checkbox			: "chkLog",
        Pattern_level			: /^level\[[\d]+\]/,
        Pattern_stat			: /^stats\[[\d]+\]/,
        Pattern_item			: /^items\[[\d]+\]/,
        Pattern_detail			: /^details\[[\d]+\]/,
        Pattern_checkboxName	: /^chkLog/,
        Pattern_logRow			: /^row\d/,
        Pattern_idNumber		: /([\d]+)/,
        pattern_http			: /^http/i		
		}
    //-----------------------------------------------------------------------------
    // "main"
    //-----------------------------------------------------------------------------    
    function Main()
    {
        rLocal = GetLocalContents(rContents);
	    if (rLocal === null) return;
 
        var allH1 = document.getElementsByTagName("h1");
        var i = 0;
        var h1;
        var shouldContinue = false;
        if(allH1==='undefined')
            return;
        for (i = 0; i < allH1.length; ++i) {
            h1 = allH1[i]; 
            if(h1.innerHTML == rLocal.OrigText_H1_DungeonLog)
            {
                infodiv = document.createElement("div");
        		infodiv.innerHTML = "";
        		h1.parentNode.insertBefore(infodiv, h1.nextSibling);
				InsertButton(h1,rLocal.Text_Button_Exportlog,exportLog);
				InsertButton(h1,rLocal.Text_Button_ClearAll,cleartAll);
				InsertButton(h1,rLocal.Text_Button_SelectAll,selectAll);
				gResponseDiv = document.createElement("div");
        		gResponseDiv.innerHTML = "";
                gIndexTemplateDiv = document.createElement("div");
        		gIndexTemplateDiv.innerHTML = "";
                
                shouldContinue = true;
                break;
            }
        }
        if(!shouldContinue)
            return;
        var allTable = document.getElementsByTagName("table");
		for (i = 0; i < allTable.length; ++i) {
            var theTable = allTable[i]; 
            if(theTable.getAttribute("class") == "content_table")
            {
				gIndexTemplateDiv.innerHTML = theTable.outerHTML;
                prepareIndexPageTemplate();
                var allRow = theTable.getElementsByTagName("tr");
                for(var j=0; j < allRow.length; ++j)
                {
					var row = allRow[j];
                    var newCheckbox = document.createElement("input");
                    newCheckbox.setAttribute("type", "checkbox");
                    if(rVaule.Pattern_logRow.test(row.getAttribute("class")))
                    {
                        var reportName = "<span>" + row.cells[1].firstChild.innerHTML + "</span>";
                        var reportTime = "<span>" + row.cells[0].firstChild.innerHTML + "</span>";
                        var title = reportName + "&nbsp;-&nbsp;" + reportTime;
                        var allInput = row.cells[2].getElementsByTagName("input");
                        var id = "";
                        var index = "";
                        for(var k = 0; k < allInput.length; ++k)
                        {
							var input = allInput[k];
                            var name = input.getAttribute("name");
                            var value = input.getAttribute("value");
                            if(name.indexOf("report_id") != -1)
                            {
                                var Result = rVaule.Pattern_idNumber.exec(name);
                                index = Number(Result[1]);
                                id=value;
                                break;
                            }
                        }
                        newCheckbox.setAttribute("name", rVaule.Text_Checkbox + "[" + index + "]");
                        newCheckbox.setAttribute("id", rVaule.Text_Checkbox + "[" + index + "]");
                        newCheckbox.setAttribute("value", id);
                        newCheckbox.setAttribute("title", title);
                        newCheckbox.setAttribute("reportname", reportName);
                        newCheckbox.setAttribute("reporttime", reportTime);
                        newCheckbox.setAttribute("maxLevel", 1);
                        newCheckbox.setAttribute(rVaule.Text_Item, rVaule.Text_Item + "%5B" + index + "%5D");
                        newCheckbox.setAttribute(rVaule.Text_Stat, rVaule.Text_Stat + "%5B" + index + "%5D");
						row.cells[0].insertBefore(newCheckbox,row.cells[0].firstChild);
                    }
                }
                break;
            }
        }
    }

    try {Main();} catch(e) {alert("Main(): " + e);}
})()